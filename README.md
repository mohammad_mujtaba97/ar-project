# AR Project



## Step 1: Install Unity Hub

https://unity.com/download


## Step 2: Install Unity Editor version 2022.3.0f1 or later

Install Editor from inside Unity Hub or use the link:
https://unity.com/releases/editor/whats-new/2022.3.0
Make sure to include Android Build Support

## Step 3: Download AR_Project.zip from the gitlab repo

Download and extract AR_Project.zip

## Step 4: Add and Open the project.

In Unity Hub add the project AR_Project by selecting 'Open' in the 'Projects' tab and navigating to the AR_Project folder


## Step 5: Build and Test

In Build Settings make sure the platform is Android and all necessary scenes are included, then Build the apk.
Upload apk to google drive and install on any android phone.

